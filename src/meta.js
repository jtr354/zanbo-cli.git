const template = {
  'vue2.0': 'https://github.com/JTR354/vue-template2.0.git',
  'mpvue2.0': 'https://github.com/JTR354/mpvue-template.git',
  'vue2.x': 'https://github.com/JTR354/vue-template2.x.git',
  'mpvue2.x': 'https://github.com/JTR354/mpvue-template2.x.git',
  'uni-app1.0': 'https://github.com/JTR354/uni-app-template.git'
}

module.exports = {
  template
}
